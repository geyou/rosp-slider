import {Component, Inject, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: "app-grading",
  templateUrl: "./grading.component.html",
  styleUrls: ["./grading.component.css"]
})
export class GradingComponent implements OnInit {
  disabled = false;
  max = 10;
  min = 0;
  step = 1;
  thumbLabel = true;
  value: number | undefined;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit(): void {
    this.value = this.data.grade;
  }

}

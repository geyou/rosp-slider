import {NgModule} from "@angular/core";
import {MatCardModule} from "@angular/material/card";
import {MatDialogModule} from "@angular/material/dialog";
import {MatInputModule} from "@angular/material/input";
import {MatSliderModule} from "@angular/material/slider";
import {MatButtonModule} from "@angular/material/button";

@NgModule({
  declarations: [],
  exports: [MatCardModule, MatDialogModule, MatInputModule, MatSliderModule, MatButtonModule]
})
export class MaterialModule {}

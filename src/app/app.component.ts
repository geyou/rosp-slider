import {Component} from "@angular/core";
import {FormBuilder, Validators} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {GradingComponent} from "./modal-window/grading/grading.component";
import {MessageComponent} from "./modal-window/message/message/message.component";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  disabled = true;
  max = 10;
  min = 0;
  step = 1;
  thumbLabel = true;
  grade = 0;
  form = this.fb.group({
    grade: [0, [Validators.required, Validators.minLength(this.min), Validators.maxLength(this.max), Validators.pattern("^[0-9]*$")]
    ]});

  constructor(public dialog: MatDialog, private fb: FormBuilder) {}

  openDialog(): void {
      if (!this.form.valid) {
      this.alert();
      return;
    }
    this.dialog.open(GradingComponent, { data: { grade: this.form.value.grade } }).afterClosed().subscribe(result => {
      if (result === "" || result === undefined) {
        return;
      }
      this.grade = result;
    });
  }

  alert(): void {
    this.dialog.open(MessageComponent).afterClosed().subscribe(() => {
      this.form.setValue({ grade: 0 });
      this.openDialog();
    });
  }

}

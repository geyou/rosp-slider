# ROSP-Slider

A simple angular + material project to test grading in ROSP with a slider

## Run the project 
First execute the following command to install dependencies: <br />
npm ci <br />
Then serve the app on localhost (defautl IP) or using an IP address other than the default one: <br />
ng serve --host <ip_address> <br />
